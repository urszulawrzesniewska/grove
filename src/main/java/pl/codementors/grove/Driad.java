package pl.codementors.grove;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "driads")

public class Driad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private int power;

    @ManyToOne
    @JoinColumn(name = "tree", referencedColumnName = "id")
    private Tree tree;


    public Driad() {
    }

    public Driad(String name, int power, Tree tree) {
        this.name = name;
        this.power = power;
        this.tree = tree;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Tree getTree() {
        return tree;
    }

    public void setTree(Tree tree) {
        this.tree = tree;
    }

    @Override
    public String toString() {
        return "Driad{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", power=" + power +
                ", tree=" + tree +
                '}';
    }
}
