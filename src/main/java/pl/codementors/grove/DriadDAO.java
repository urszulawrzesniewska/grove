package pl.codementors.grove;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class DriadDAO {

    public Driad persist(Driad driad) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(driad);
        tx.commit();
        em.close();
        return driad;
    }
    public void delete(Driad driad) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Driad d = em.find(Driad.class, driad.getId());
        d.getTree().getDriadList().remove(d);
        em.remove(d);
        tx.commit();
        em.close();
    }
    public Driad merge(Driad driad) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        driad = em.merge(driad);
        tx.commit();
        em.close();
        return driad;
    }

    public List<Driad> findAll() {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        List<Driad> driads = em.createQuery("select d from Driad d").getResultList();
        em.close();
        return driads;
    }
}
