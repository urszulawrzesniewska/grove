package pl.codementors.grove;

import java.util.Scanner;

public class JpaMain {
    public static void main(String[] args) {

        TreesDAO dao = new TreesDAO();

        Tree fir = new Tree("Fir", 12);
        Tree birch = new Tree("Birch", 32);
        Tree ash = new Tree("Ash", 11);


        DriadDAO driadDAO = new DriadDAO();

        Driad driad = new Driad("Ania",14,birch);


        while (true) {
            System.out.println("Choose action: ");
            System.out.println("1. Add trees");
            System.out.println("2. Remove trees");
            System.out.println("3. Show tree list");
            System.out.println("4. Change tree details");
            System.out.println("5. Add driad");
            System.out.println("6. Modify driad");
            System.out.println("7. List driads");
            System.out.println("8. Remove driad");
            System.out.println("9. Quit");

            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();

            if (choice.equals("1")) {
                System.out.println("Add trees");
                dao.persist(birch);
                dao.persist(fir);
                dao.persist(ash);

            }
            if (choice.equals("2")) {

                System.out.println("Remove trees");
                dao.delete(birch);
                dao.delete(fir);
                dao.delete(ash);
            }
            if (choice.equals("3")) {
                System.out.println("Showing tree list");
                dao.findAll().forEach(System.out::println);
            }
            if (choice.equals("4")) {
                System.out.println("Amend tree details");
                birch.setHeight(99);
                dao.merge(birch);
            }
            if(choice.equals("5")) {
                System.out.println("Adding driad");
                driad=driadDAO.persist(driad);
            }
            if (choice.equals("6")) {
                System.out.println("Modifying driad");
                driad.setName("Basia");
                driad.setTree(ash);
                driadDAO.merge(driad);
            }
            if (choice.equals("7")) {
                System.out.println("Listing driads");
                driadDAO.findAll().forEach(System.out::println);
            }
            if (choice.equals("8")) {
                System.out.println("Remove driad");
                driadDAO.delete(driad);
            }
            if (choice.equals("9")) {
                System.out.println("Quit");
                break;
            }

        }
            TreesEntityManagerFactory.close();

    }
}
