package pl.codementors.grove;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "trees")

public class Tree {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String species;

    @Column
    private int height;

    @OneToMany(mappedBy = "tree", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Driad> driadList;


    public Tree() {

    }
    public Tree(String species, int height) {
        this.species = species;
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<Driad> getDriadList() {
        return driadList;
    }

    public void setDriadList(List<Driad> driadList) {
        this.driadList = driadList;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "id=" + id +
                ", species='" + species + '\'' +
                ", height=" + height +
                '}';
    }
}
