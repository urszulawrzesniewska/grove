package pl.codementors.grove;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class TreesDAO {

    public Tree persist(Tree tree) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(tree);
        tx.commit();
        em.close();
        return tree;
    }

    public Tree merge(Tree tree) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        tree = em.merge(tree);
        tx.commit();
        em.close();
        return tree;
    }

    public void delete(Tree tree) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.find(Tree.class, tree.getId()));
        tx.commit();
        em.close();
    }

    public Tree find(int id) {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        Tree tree = em.find(Tree.class, id);
        em.close();
        return tree;
    }

    public List<Tree> findAll() {
        EntityManager em = TreesEntityManagerFactory.createEntityManager();
        List<Tree> trees = em.createQuery("select t from Tree t").getResultList();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tree> query = cb.createQuery(Tree.class);
        query.from(Tree.class);
        em.close();
        return trees;
    }
}
