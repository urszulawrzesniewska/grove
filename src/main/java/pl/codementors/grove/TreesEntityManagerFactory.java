package pl.codementors.grove;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class TreesEntityManagerFactory {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("GrovePU");

    public static final EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public static void close() {
        emf.close();
    }
}
