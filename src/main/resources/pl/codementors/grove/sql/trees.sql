CREATE USER 'ula' IDENTIFIED BY 'siatkowka';

CREATE DATABASE grove CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON grove.* TO 'ula';
USE grove;

CREATE TABLE trees (
    id INT NOT NULL AUTO_INCREMENT,
    species VARCHAR(256),
    height INT,
    PRIMARY KEY (id)
);